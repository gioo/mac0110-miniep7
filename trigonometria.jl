# MAC0110 - MiniEP7
# Giovani Tavares de Andrade - 10788620
function fatorial(n)
    if n==0
        return 1
    else
        return n*fatorial(BigInt(n-1))
    end
end

function serie_taylor(x,funcao)

    if funcao=="sin"
        if x==0
            return 0
        end
        fat=1
    end

    if funcao=="cos"
        if x==0
            return 1
        end
        fat=0
    end

    iteracao=1
    resposta=0
    while iteracao<=10
        termo=x^fat
        calculo_fatorial=fatorial(BigInt(fat))
        if iteracao%2==1
            resposta+=(termo/calculo_fatorial)
        else
            resposta-=(termo/calculo_fatorial)
        end
        fat+=2
        iteracao+=1
    end
    return round(Float64(resposta),digits=3)
end

function bernoulli_(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
        return abs(A[1])
end

function taylor_tan2(x)
    resposta=x
    n=2
    adicionou=1
    while abs(adicionou)>=0.00001
        fat=fatorial(BigInt(2n))
        bernoulli=bernoulli_(n)
        expo1=big(2^2n)
        expo2=big(2^(2n)-1)
        expo3=big(x^(2n-1))
        adicionou=big(expo1*expo2*expo3*bernoulli)/fat
        resposta+=adicionou
        n+=1
    end
    return round(Float64(resposta),digits=3)
end

function check_sin(value,x)
    if value!=round(sin(x),digits=3)
        return false
    end
    return true
end

function check_cos(value,x)
    if value!=round(cos(x), digits=3)
        return false
    end
    return true
end

function check_tan(value, x)
    if value!=round(tan(x),digits=3)
        return false
    end
    return true
end

function test()
    erro=false
    for x in [0,1,pi/4,pi/6,pi/1000000,-1,-pi/4,-pi/6,-pi/1000000]
        if !check_sin(taylor_sin(x),x)
            println("Problema na função seno")
            erro=true
        end
        if !check_cos(taylor_cos(x),x)
            println("Problema na função cosseno")
            erro=true
        end
        if !check_tan(taylor_tan(x),x)
            println("Problema na função tangente")
            erro=true
        end
    end
    if !erro
        println("Tudo OK :)")
    end
end

function taylor_sin(x)
    return serie_taylor(x,"sin")
end

function taylor_cos(x)
    return serie_taylor(x,"cos")
end

function taylor_tan(x)
    return taylor_tan2(x)
end
